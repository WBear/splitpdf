package rs.enetel.pdfSplit;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.junit.Test;

public class PdfSplitTest {
	
	//SINGLE FILE ITERATION
	@Test
	public void getFirstPageSingleFileIterations() throws IOException
	{
		long startTime = System.currentTimeMillis();
		PDDocument existingDocument = null;
		PDDocument newDocument = null;
        try
        {
        	existingDocument = PDDocument.load( new File("src/main/resources/052014-00000038421_Installments-other+Itemization.pdf") );
            if( existingDocument.isEncrypted() )
            {
                throw new IOException( "Encrypted documents are not supported" );
            }
            if( existingDocument.getNumberOfPages() <= 1 )
            {
                throw new IOException( "Error: A PDF document must have at least one page, " +
                                       "cannot get the first page!");
            }

	        for (int j = 1; j <= 10000; j++)
	        {
	            newDocument = new PDDocument();
	            newDocument.addPage((PDPage) existingDocument.getDocumentCatalog().getPages().get(0));   
	            newDocument.save("src/main/resources/original_invoice_test3/2_10000_" + j + "_052014-00000038421_Installments-other+Itemization.pdf");  
	            newDocument.close();  
	        }
        }
        finally
        {
            if( existingDocument != null )
            {
            	existingDocument.close();
                long endTime = System.currentTimeMillis();
                long timeElapsed = endTime - startTime;
                long second = (timeElapsed / 1000) % 60;
                long minute = (timeElapsed / (1000 * 60)) % 60;
                System.out.println( String.format("%s:%s:%s", minute, second, timeElapsed) );
                System.out.println( timeElapsed );
            }
        }
	}
	
	
	//DIRECTORY FILES ITERATION (COPY TO NEW DIRECTORY)
	@Test
	public void copyFilesDiffrentDirectory() throws IOException
	{	
		PDDocument existingDocument = null;
		PDDocument newDocument = null;
		
		long startTime = System.currentTimeMillis();
		
		final File folder = new File("src/main/resources/input_invoice_10000");
		
		for (final File fileEntry : folder.listFiles()) {
			
			try
			{
				existingDocument = PDDocument.load( new File(folder + "/" + fileEntry.getName()) );
				if( existingDocument.isEncrypted() )
				{
					throw new IOException( "Encrypted documents are not supported" );
				}
				if( existingDocument.getNumberOfPages() <= 1 )
				{
					throw new IOException( "Error: A PDF document must have at least one page, " +
							"cannot get the first page!");
				}
				
				newDocument = new PDDocument();
				newDocument.addPage((PDPage) existingDocument.getDocumentCatalog().getPages().get(0));   
				newDocument.save("src/main/resources/output_invoice_10000/1_" + fileEntry.getName());  
				newDocument.close();  
			}
			finally
			{
				if( existingDocument != null )
				{
					existingDocument.close();
				}
			}
		}
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime - startTime;
		long second = (timeElapsed / 1000) % 60;
		long minute = (timeElapsed / (1000 * 60)) % 60;
		System.out.println( String.format("%s:%s:%s", minute, second, timeElapsed) );
		System.out.println( timeElapsed );
	}
	
	//DIRECTORY FILES ITERATION (OVERWRITE ORIGINAL FILES)
	//NOT WORKING !
	public void overwriteFilesSameDirectory() throws IOException
	{	
		long startTime = System.currentTimeMillis();
		
		final File folder = new File("src/main/resources/");
		
		for (final File fileEntry : folder.listFiles()) {
			
			PDDocument existingDocument = null;
			PDDocument newDocument = null;
			
			try
			{
				existingDocument = PDDocument.load( new File(folder + "/" + fileEntry.getName()) );
				
				if( existingDocument.isEncrypted() )
				{
					throw new IOException( "Encrypted documents are not supported" );
				}
				if( existingDocument.getNumberOfPages() <= 1 )
				{
					throw new IOException( "Error: A PDF document must have at least one page, " +
							"cannot get the first page!");
				}
				
				PDPage firstPage = existingDocument.getDocumentCatalog().getPages().get(0);
				String newFilename = fileEntry.getName();
				existingDocument.close();
				
				newDocument = new PDDocument();
				newDocument.addPage(firstPage);
				newDocument.save("src/main/resources/" + newFilename);  
				newDocument.close();  
			}
			finally
			{
				if( existingDocument != null )
				{
					
				}
			}
		}
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime - startTime;
		long second = (timeElapsed / 1000) % 60;
		long minute = (timeElapsed / (1000 * 60)) % 60;
		System.out.println( String.format("%s:%s:%s", minute, second, timeElapsed) );
		System.out.println( timeElapsed );
	}
	
	@Test
	public void copyFileToDirectory() throws IOException
	{
		long startTime = System.currentTimeMillis();
		PDDocument existingDocument = null;
		PDDocument newDocument = null;
        try
        {
        	existingDocument = PDDocument.load( new File("src/main/resources/052014-00000002051_Office-Complete.pdf") );
            if( existingDocument.isEncrypted() )
            {
                throw new IOException( "Encrypted documents are not supported" );
            }
            if( existingDocument.getNumberOfPages() <= 1 )
            {
                throw new IOException( "Error: A PDF document must have at least one page, " +
                                       "cannot get the first page!");
            }

	        for (int j = 1; j <= 10000; j++)
	        {
	            newDocument = existingDocument;
	            newDocument.save("src/main/resources/input_invoice_10000/" + j + "_052014-00000002051_Office-Complete.pdf");  
	            //newDocument.close();  
	        }
        }
        finally
        {
            if( existingDocument != null )
            {
            	existingDocument.close();
                long endTime = System.currentTimeMillis();
                long timeElapsed = endTime - startTime;
                long second = (timeElapsed / 1000) % 60;
                long minute = (timeElapsed / (1000 * 60)) % 60;
                System.out.println( String.format("%s:%s:%s", minute, second, timeElapsed) );
                System.out.println( timeElapsed );
            }
        }
	}
}
